package com.tacosteemers.objectcreation;

import static com.tacosteemers.objectcreation.ExpensiveResource.getExpensiveResource;

public class Example {

    /*  Here we look at different ways that objects can be created.
        Keywords are:
        - declaration
        - initialization
        - assignment
        - values
        - references
        - impact on garbage collection

        In this file we see five ways that we can create objects,
        and we talk about long they are kept in memory.

        As programmers, we often we talk about calling a method and getting an object in return.
        This is a quick way to describe what is happening, but it is not very precise.
        Here follows a simplified and incomplete explanation for Java.

        We create objects because we want to achieve something.
        We may want to acquire some data from somewhere.
        If we say that we have an object, we don't really have that data.
        Instead, we have a value that is a reference to a location in memory,
        and the contents of the object we are talking about can be found there.

        A variable is a name that refers to a value.
        That value is called a reference.
        A reference is an address to a location in memory.
        If we want to access an object, the variable can refer us to the correct location in memory.
        That is where the name "reference" comes from.
        At that location is the start of the memory space that contains everything that the object consists of.
        Some of these things are also references to other places in memory.

        When we discuss objects in Java, we also need to discuss garbage collection.
        A simplified explanation is provided here, but a better explanation can be found at:
            https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/gc01/index.html

        The Garbage Collector frees up working memory by collecting our garbage.
        Objects are created all the time.
        At some point our Java application might start to run out of space.
        Most recently created objects can probably be considered garbage;
        they were created and used but now no variable is referencing them anymore.
        The Garbage Collector will check to see if there are any references to an object.
        If not, the Garbage Collector will delete that object.
        The way we create objects and make them available has an effect on how much working memory can be freed up.
     */

    /*  First example:

        static final ExpensiveResource a = getExpensiveResource();

        Here we declare that we want to hold any object of type ExpensiveResource,
        and we want to refer to that object with the name "a".
        We call this "variable a" because generally speaking the contents of "a" can vary during the lifetime
        of the variable.
        However, here we declare that this ExpensiveResource is "final",
        the exact ExpensiveResource we refer to will not change, though it's contents might change.

        After declaring "a" we immediately initialize it.
        In other words, we immediately assign an initial value to the variable called "a".
        We cannot assign something else to "a" later on, because it is marked as "final".

        Variable "a" is marked as static.
        As a result, "a" exists separate from any instance of the Example class.
        "a" will be initialized when this class is first used in any way.
        There will only be one variable "a" as long as this class is used anywhere.

        Object "a" cannot be garbage collected as long as this class is used anywhere.
        That is important to realise, because the object will probably end up using
        memory for almost the entire lifetime of the application.
     */
    private static final ExpensiveResource a = getExpensiveResource();

    public static ExpensiveResource getA() {
        return a;
    }



    /*  final ExpensiveResource b = getExpensiveResource();

        Here we declare and immediately initialize "b".
        Initializing simply means assigning an initial value to the variable called "b", in this case that value is final.
        WHen we talk about a value here, we are talking about a value that contains an address in memory,
        and at that address we find our ExpensiveResource object.

        Object "b" will be created when an instance of the class Example is created.
        There will be one object "b" per instance of this class.

        Object "b" cannot be garbage collected as long as this instance is used anywhere.
        For each instance of this class we will be taking up space in memory to
        store this object. We only get this space back if the instance is garbage collected.
     */
    private final ExpensiveResource b = getExpensiveResource();

    public ExpensiveResource getB() {
        return b;
    }



    /*  Here we declare a variable called "c".
        We don't initialize it here; it is initialized in the constructor instead.
        "c" is again final, and there will be one "c" per instance of Example.
     */
    private final ExpensiveResource c;

    // The constructor initializes "c".
    public Example() {
        c = getExpensiveResource();
    }

    public ExpensiveResource getC() {
        return c;
    }



    /*  ExpensiveResource d;

        Here we declare a variable called "d".
        We don't initialize it; instead it is assigned when the "setD" method is called.

        Each instance of the Example class will have one variable "d".
        However, variable "d" may not have been assigned any value.
        "d" may not be referring to any object, it may be null.
        A small amount of memory is required just because each instance of Example has
        declared variable "d", but other than that variable "d"
        only has an effect on memory usage and garbage collection if it is assigned to by calling "setD".
     */
    private ExpensiveResource d;

    public ExpensiveResource getD() {
        return d;
    }

    // Here we assign a new value to the variable called "d". We can do that because "d" is not "final".
    public void setD(ExpensiveResource d) {
        this.d = d;
    }



    /*  Finally, there is also a way to get a value from this class that is not assigned to any variable.

        That is what happens in the "getE" method.
        The object that we return here is created every time this method is called.

        There is no variable "e" that gets declared, initialized or assigned.
        We simply instantiate an object of a specific class,
        and return the value of the reference to that object to the caller of the "getE" method.
        Whether the result of "getE" can be garbage collected depends on whether the caller is storing that
        reference or not.
    */
    public ExpensiveResource getE() {
        return getExpensiveResource();
    }

    /*  Because the "getE" method does not use anything that belongs to an instance of the
        Example class, we can mark "getE" as static. Let's call that static version "getF".

        We don't need an instance of Example to call "getF" because "getF" is static.
        We can simply call Example.getF();
     */
    public static ExpensiveResource getF() {
        return getExpensiveResource();
    }

}
