package com.tacosteemers.objectcreation;

public class ExpensiveResource {


    public static ExpensiveResource getExpensiveResource() {
        // Imagine an expensive process here.
        // Also imagine that the resulting object takes up a lot of memory.
        return new ExpensiveResource();
    }
}
