# Java examples

The examples use Java 11.
The examples can be built with gradle or the provided gradle wrapper:
`./gradlew clean build run`
That notation works for Linux and macOS, but on Windows it may need a slightly different notation.

## Creating objects
Object creation, covering:
- declaration
- initialization
- assignment
- values
- references
- impact on garbage collection

[Source](https://gitlab.com/taco.steemers/examples/-/tree/main/object_creation), [YouTube video](https://www.youtube.com/watch?v=t5186p7T1yo).

This is example code and not an example that can be run.

## Mapped Diagnostic Context

### For any type of application
Mapped Diagnostic Context (MDC) for event tracking: [Source](https://gitlab.com/taco.steemers/examples/-/tree/main/eventtracking), [Youtube video](https://www.youTube.com/watch?v=hSIUDOFghNg).

This example can be built and run from the project root:
`./gradlew clean build run -p eventtracking`

### With a Filter, for Java Enterprise Edition web applications

Spring Boot request tracking example using the MDC: [Source](https://gitlab.com/taco.steemers/examples/-/tree/main/springbootmdc), [Youtube video](https://www.youtube.com/watch?v=Gz3PX2254zo).

This example can be built and run from the project root:
`./gradlew clean build run -p springbootmdc`

## Java Standard Edition with context dependency injection
The context dependency injection (CDI) specification is part of Java Enterprise Edition (JavaEE).
By using the JBoss Weld CDI implementation we can use it in a commandline or graphical user interface application
that would normally be made using just the Java Standard Edition (JavaSE).
This is an example project where we can see how to benefit from CDI without having to use
a heavy framework like Spring with a full JavaEE implementation.

[Source](https://gitlab.com/taco.steemers/examples/-/tree/main/java_se_context_dependency_injection)

This example can be built from the project root:
`./gradlew clean build installDist -p java_se_context_dependency_injection`

On Linux or macOs we can run it by calling this script:
`./java_se_context_dependency_injection/build/install/java_se_context_dependency_injection/bin/java_se_context_dependency_injection`

On Windows we have to call the bat file instead:
`java_se_context_dependency_injection/build/install/java_se_context_dependency_injection/bin/java_se_context_dependency_injection.bat`

For the other runnable examples we can use the "clean build run" gradle tasks to build and start the example.
Why do we need to run the installDist gradle task and call a shell script to start the application?
This is because Weld requires the beans to be in a so-called ["bean archive"](https://docs.jboss.org/weld/reference/latest/en-US/html/ee.html#packaging-and-deployment),
and that makes the "installDist" gradle task the easiest way to run this example.
Spring Boot works around this in its own way, and that is why we can run the Spring Boot request tracking example
by calling the "clean build run" gradle tasks.
