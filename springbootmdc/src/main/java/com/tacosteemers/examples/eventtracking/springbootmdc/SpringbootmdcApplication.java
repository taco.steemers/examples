package com.tacosteemers.examples.eventtracking.springbootmdc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootmdcApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootmdcApplication.class, args);
	}

}
