package com.tacosteemers.examples.eventtracking.springbootmdc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/2")
public class EndpointTwo {

    @GetMapping
    @ResponseBody
    public void two() throws Exception {
        throw new Exception("This exception comes from the HelloController");
    }
}
