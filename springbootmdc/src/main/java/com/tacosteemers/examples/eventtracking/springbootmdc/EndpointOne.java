package com.tacosteemers.examples.eventtracking.springbootmdc;

import org.slf4j.MDC;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/1")
public class EndpointOne {

    @GetMapping
    @ResponseBody
    public String one() {
        return "Hello, this request has identifier " + MDC.get("reqId");
    }
}
