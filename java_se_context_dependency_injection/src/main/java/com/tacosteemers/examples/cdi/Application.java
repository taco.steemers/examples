package com.tacosteemers.examples.cdi;

import org.jboss.weld.environment.se.events.ContainerInitialized;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

@ApplicationScoped
public class Application {

    @Inject
    private Configuration configuration;

    @Inject
    private SomeService someService;

    public void containerInitializedEventHandler(@Observes ContainerInitialized event) {
        System.out.println("Initialization - ContainerInitialized Application");
    }

    @PostConstruct
    public void initialize() {
        System.out.println("Initialization - PostConstruct Application");
    }

    @PreDestroy
    public void stop() {
        System.out.println("Shutdown - PreDestroy Application");
    }

    public void start(String[] args) {
        System.out.println("Running - Application start method has been called");
        System.out.print("Running - The start method calls someService.isObjectSet(): ");
        System.out.println(someService.isObjectSet());
        System.out.println("Running - Reached the end of the start method");
    }
}
