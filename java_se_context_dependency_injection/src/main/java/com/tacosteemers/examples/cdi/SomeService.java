package com.tacosteemers.examples.cdi;

import org.jboss.weld.environment.se.events.ContainerInitialized;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

@ApplicationScoped
public class SomeService {

    @Inject
    private Configuration configuration;

    private Object object = null;

    public void containerInitializedEventHandler(@Observes ContainerInitialized event) {
        System.out.println("Initialization - ContainerInitialized SomeService");
    }

    @PostConstruct
    public void init() {
        System.out.println("Initialization - PostConstruct SomeService");
        // The Configuration object will be available if we want to use it here
        System.out.println("Initialization - SomeService is calling configuration.getSomething()");
        object = configuration.getSomething();
    }

    @PreDestroy
    public void stop() {
        System.out.println("Shutdown - PreDestroy SomeService");
    }

    public boolean isObjectSet() {
        return object != null;
    }
}
