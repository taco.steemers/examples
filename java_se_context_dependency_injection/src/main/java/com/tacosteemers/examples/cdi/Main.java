package com.tacosteemers.examples.cdi;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

public class Main {

    private Weld weld;
    private WeldContainer container;
    private Application application;

    private static String[] applicationArguments;

    private void init() {
        weld = new Weld();
        container = weld.initialize();
        application = container.select(Application.class).get();
    }

    private void start() {
        application.start(applicationArguments);
    }

    private void shutdown() {
        weld.shutdown();
    }

    public static void main(String[] args) {
        applicationArguments = args;

        Main main = new Main();
        main.init();
        main.start();
        main.shutdown();
    }
}
