package com.tacosteemers.examples.cdi;

import org.jboss.weld.environment.se.events.ContainerInitialized;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class Configuration {

    public void containerInitializedEventHandler(@Observes ContainerInitialized event) {
        System.out.println("Initialization - ContainerInitialized Configuration");
    }

    @PostConstruct
    public void initialize() {
        System.out.println("Initialization - PostConstruct Configuration");
    }

    @PreDestroy
    public void stop() {
        System.out.println("Shutdown - PreDestroy Configuration");
    }

    public Object getSomething() {
        System.out.println("Initialization/Running - Configuration getSomething method has been called");
        return new Object();
    }
}
