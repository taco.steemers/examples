package com.tacosteemers.examples.eventtracking;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private static final String CONTEXT_TYPE_KEY = "type";
    private static final String CONTEXT_TYPE_USER_EVENT = "user_event";
    private static final String CONTEXT_TYPE_SYSTEM_EVENT = "system_event";

    public static void main(String[] args) {
        MDC.put(CONTEXT_TYPE_KEY, "Main");
        /*
        "Simplest possible example for tracking events with the MDC."
        MDC stands for Mapped Diagnostic Context.
        We use the MDC class from SLF4J. SLF4J is the logging API. Logback, the logging implementation, supports MDC.
        It is a map that is always available throughout the life-time of the thread.
        We use logback to log a value from the MDC.
        See logback.xml where we see that log statements will contain the current value for "type".
        Let's run the application.
        The first 5 threads have a previous value of null. Not "Main", as we have set above.
        This is because the thread pool is of size 5.
        There are 5 new threads scheduled to run. They have their own MDC.
        Logging from later threads show that the MDC content changes over time because new runnables
        were scheduled on the same 5 threads.
        We could have cleared the MDC with MDC.clear() at the entrypoint.
        As you can imagine, we can add a lot of useful context to log statements with the MDC.
        The interesting part is that we don't have to add the context to the log statements by hand.
        We only need to set them upon entry.
        An example entry point in a web application could be the doFilter method in a javax.servlet.Filter class.
        It is possible to make a copy of the MDC, which would be handy for passing to a different thread:
        MDC.getCopyOfContextMap()
         */

        ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(5);
        for (int i = 0; i < 10; i++) {
            Runnable systemType = () -> entryPoint(CONTEXT_TYPE_SYSTEM_EVENT);
            Runnable userType = () -> entryPoint(CONTEXT_TYPE_USER_EVENT);
            scheduledExecutorService.schedule(systemType, 100 - i, TimeUnit.MILLISECONDS);
            scheduledExecutorService.schedule(userType, 100 - i, TimeUnit.MILLISECONDS);
        }
        LOGGER.info("Finished scheduling threads.");
    }

    public synchronized static void entryPoint(String callerType) {

        String mdcPreviousValue = MDC.get(CONTEXT_TYPE_KEY);
        MDC.put(CONTEXT_TYPE_KEY, callerType);

        LOGGER.info("Previous value was {}", mdcPreviousValue);
    }
}